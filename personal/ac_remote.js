function get_dec_name()
{
	return 'AC Remote';
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()
{
	var i;
	
	ui_clear();
	
	ui_add_ch_selector('ch', 'Channel', 'IR');
	
	ui_add_txt_combo('msb', 'Endianess');
	ui_add_item_to_txt_combo('LSB', true);
	ui_add_item_to_txt_combo('MSB');
	
	ui_add_num_combo('tolerance', 'Tolerance (%)');
	for (i = 1; i <= 100; i++) {
		ui_add_item_to_txt_combo(i, i === 20);
	}
	
	ui_add_info_label('All values below are in microseconds');

	ui_add_num_combo('init_low', 'INIT Falling');
	for (i = 100; i <= 20000; i += 100) {
		ui_add_item_to_txt_combo(i, i === 10000);
	}

	ui_add_num_combo('init_high', 'INIT Rising');
	for (i = 100; i <= 20000; i += 100) {
		ui_add_item_to_txt_combo(i, i === 10000);
	}

	ui_add_num_combo('data_t1', 'DATA Falling');
	for (i = 10; i <= 2000; i += 10) {
		ui_add_item_to_txt_combo(i, i === 1000);
	}

	ui_add_num_combo('data_t2_zero', 'DATA Rising 0');
	for (i = 10; i <= 2000; i += 10) {
		ui_add_item_to_txt_combo(i, i === 1000);
	}

	ui_add_num_combo('data_t2_one', 'DATA Rising 1');
	for (i = 10; i <= 2000; i += 10) {
		ui_add_item_to_txt_combo(i, i === 1000);
	}
	
	ui_add_num_combo('pause', 'PAUSE Rising');
	for (i = 100; i <= 20000; i += 100) {
		ui_add_item_to_txt_combo(i, i === 10000);
	}
}

function sampleToUsec(sample) {
	return sample / sample_rate * 1e6;
}

function inRange(value, targetValue) {
	var margin = targetValue * tolerance / 100;
	
	if (
		value < (targetValue - margin) ||
		value > (targetValue + margin)
	) {
		return 0;
	}

	return Math.abs(value - targetValue) || 1;
}

function dec2hex(num) {
	return (num < 0x10 ? '0' : '') + num.toString(16).toUpperCase();
}

function decode()
{
	// Update the content of user interface variables
    get_ui_vals();
	msb = parseInt(msb);
	init_low = parseInt(init_low) * 100;
	init_high = parseInt(init_high) * 100;
	data_t1 = parseInt(data_t1) * 10;
	data_t2_zero = parseInt(data_t2_zero) * 10;
	data_t2_one = parseInt(data_t2_one) * 10;
	pause = parseInt(pause) * 100;
	
	// Clears all the the decoder items and its content
	clear_dec_items();

    var tPrev;
	var tOld = trs_get_first(ch);
	
	// 0 = before init (high)
	// 1 = init fall
	// 2 = init rise
	// 3 = data fall
	// 4 = data rise
	// 5 = error
	var state = 0;
	
	var byteStart = 0;
	var bytes = [];
	var values = [];
	var currByte = '';
	var colors = dark_colors;
	
	while (trs_is_not_last(ch)) {
		if (abort_requested()) {
			break;
		}

		t = trs_get_next(ch);

		//debug(t.val + ' @ ' + t.sample + ' (' + (sampleToUsec(t.sample) / 10e2).toFixed(3) + ' ms)', t.sample);

		var delta = t.sample - tOld.sample;
		var deltaUs = sampleToUsec(delta);
		
		//debug(delta + ' ' + deltaUs);
		
		if (state === 0 && t.val === 0) {
			state = 1;
		}

		if ([1, 3].indexOf(state) > -1 && t.val === 1 && inRange(deltaUs, init_low)) {
			byteStart = 0;
			currByte = '';
			state = 2;
		}
		
		if (state === 2 && t.val === 0 && inRange(deltaUs, init_high)) {
			byteStart = 0;
			currByte = '';
			state = 3;
			
			dec_item_new(ch, tPrev.sample, t.sample);
			dec_item_add_pre_text('Init Pulse');
			dec_item_add_pre_text('INIT');
			dec_item_add_pre_text('I');
			
			pkt_start();
			pkt_add_item(tPrev.sample, t.sample, 'INIT', '', colors.red, colors.red, 0);
			pkt_end();
		}
		
		if (state === 3 && t.val === 1 && inRange(deltaUs, data_t1)) {
			if (byteStart === 0) {
				byteStart = tOld.sample;
			}
			
			state = 4;
		}
		
		if (state === 4 && t.val === 0) {
			var isPause = inRange(deltaUs, pause);
			var isOne = inRange(deltaUs, data_t2_one);
			var isZero = inRange(deltaUs, data_t2_zero);
			var sampleMiddle = (t.sample + tPrev.sample) / 2;
			var addBit = '';

			if (isOne) {
				addBit = '1';
				dec_item_add_sample_point(ch, sampleMiddle, DRAW_1);
				state = 3;
			}
			else {
				if (isZero) {
					addBit = '0';
					dec_item_add_sample_point(ch, sampleMiddle, DRAW_0);
					state = 3;
				}
				else {
					if (pause && isPause) {
						dec_item_new(ch, tPrev.sample, t.sample);
						dec_item_add_pre_text('Pause');
						dec_item_add_pre_text('PS');
						dec_item_add_pre_text('P');
						
						pkt_start();
						pkt_add_item(tPrev.sample, t.sample, 'PAUSE', '', colors.green, colors.green, 0);
						pkt_end();
						
						byteStart = 0;
						currByte = '';
						dec_item_add_sample_point(ch, sampleMiddle, DRAW_POINT);
						state = 3;
					}
					else {
						debug(values.map(dec2hex).join(' '));
						values = [];
						
						if (trs_is_not_last(ch)) {
							dec_item_new(ch, tPrev.sample, tOld.sample);
							dec_item_add_pre_text('End');
							dec_item_add_pre_text('EN');
							dec_item_add_pre_text('E');
							
							pkt_start();
							pkt_add_item(tPrev.sample, tOld.sample, 'END', '', colors.red, colors.red, 0);
							pkt_end();
							
							byteStart = 0;
							currByte = '';
							dec_item_add_sample_point(ch, sampleMiddle, DRAW_CROSS);
							
							state = 1;
						}
					}
				}
			}
			
			if (addBit.length > 0) {
				if (msb) {
					currByte += addBit;
				}
				else {
					currByte = addBit + currByte;
				}
			}
			
			if (currByte.length === 8) {
				var value = parseInt(currByte, 2);
				currByte = '';
				
				bytes.push({
					start: byteStart,
					end: t.sample,
					value: value
				});
				
				values.push(value);
				
				dec_item_new(ch, byteStart, t.sample);
				dec_item_add_pre_text('Data Field');
				dec_item_add_pre_text('DATA');
				dec_item_add_pre_text('D');
				dec_item_add_data(value);
				
				pkt_start();
				pkt_add_item(byteStart, t.sample, 'DATA', value, colors.blue, colors.blue, 0);
				pkt_end();
				
				byteStart = 0;
			}
		}
		
		tPrev = tOld;
		tOld = t;
		set_progress(t.sample / n_samples * 100);
	}
}

