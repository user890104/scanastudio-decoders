function get_dec_name()
{
	return 'blizoo RC';
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()
{
    ui_clear();
    ui_add_ch_selector('ch', 'Channel', 'IR');
}

function sampleToUsec(sample) {
	return sample / sample_rate * 10e5;
}

function usecToSample(usec) {
	return usec / 10e5 * sample_rate;
}

function inRange(value, targetValue) {
	var tolerance = targetValue * 0.2;

	return value > (targetValue - tolerance) &&
		value < (targetValue + tolerance);
}

function formatNumber(num) {
	if (num < 10) {
		return '0' + num;
	}
	
	return num.toString();
}

function hexdig(num) {
	if (num < 0 || num > 15) {
		throw new Exception('wtf');
	}
	if (num >= 0 && num <= 9) {
		return num.toString();
	}
	return String.fromCharCode(num - 10 + 65);
}

function dec2hex(num) {
	var a = Math.floor(num / 16);
	return hexdig(a) + hexdig(num - a * 16);
}

function decode()
{
    get_ui_vals();                // Update the content of user interface variables
    clear_dec_items();            // Clears all the the decoder items and its content

	var t = trs_get_first(ch);
	var tOld = t;
	
	var byteStart = 0;
	var byteEnd = 0;
	var bytes = [];
	var currByte = '';
	var colors = dark_colors;
	
	while (trs_is_not_last(ch)) {
		if (abort_requested()) {
			break;
		}

		t = trs_get_next(ch);

		//debug(t.val + ' @ ' + t.sample + ' (' + (sampleToUsec(t.sample) / 10e2).toFixed(3) + ' ms)', t.sample);

		var delta = t.sample - tOld.sample;
		var deltaUs = sampleToUsec(delta);
		
		//debug(delta + ' ' + deltaUs);
		
		var isZero = inRange(deltaUs, 900);
		var isOne = inRange(deltaUs, 1750);
		
		if (isZero || isOne) {
			if (byteStart === 0) {
				byteStart = tOld.sample;
			}
			byteEnd = t.sample;
		}
		
		var sampleMiddle = t.sample - delta / 2;
		
		if (isZero) {
			currByte = '0' + currByte;
			dec_item_add_sample_point(ch, sampleMiddle, DRAW_0);
		}
		
		if (isOne) {
			currByte = '1' + currByte;
			dec_item_add_sample_point(ch, sampleMiddle, DRAW_1);
		}
		
		if (currByte.length === 8) {
			var value = parseInt(currByte, 2);
			debug(currByte);
			currByte = '';
			
			bytes.push({
				start: byteStart,
				end: t.sample,
				value: value
			});
			
			dec_item_new(ch, byteStart, t.sample);
			dec_item_add_pre_text('Data Field');
			dec_item_add_pre_text('DATA');
			dec_item_add_pre_text('D');
			dec_item_add_data(value);
			
			pkt_start();
			pkt_add_item(byteStart, t.sample, 'DATA', dec2hex(value), colors.red, colors.red, 0);
			pkt_end();
			
			byteStart = 0;
		}
		
		tOld = t;
		
		set_progress(100 * t.sample / n_samples);
	}
	
	if (currByte.length > 0) {
		var value = parseInt(currByte, 2);
		
		debug(currByte);
		debug(value);
		
		dec_item_new(ch, byteStart, byteEnd);
		dec_item_add_pre_text('Data Field');
		dec_item_add_pre_text('DATA');
		dec_item_add_pre_text('D');
		dec_item_add_data(value);
		
		pkt_start();
		pkt_add_item(byteStart, byteEnd, 'DATA', dec2hex(value), colors.red, colors.red, 0);
		pkt_end();
	}
	
/*	
	if (bytes.length !== 9) {
		debug('Decode error!');
		return;
	}
*/
	var bytesData = bytes.map(function(b) {
		return b.value;
	});
		
	// DEBUG
	debug(bytesData.join(' '));
	debug(bytesData.map(dec2hex).join(' '));
}
