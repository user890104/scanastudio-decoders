function get_dec_name()
{
	return 'SANG AC Remote';
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()
{
    ui_clear();
    ui_add_ch_selector('ch', 'Channel', 'IR');
}

function formatNumber(num) {
	if (num < 10) {
		return '0' + num;
	}
	
	return num.toString();
}

function formatTimer(val) {
	return val ? 'ACTIVE' : 'INACTIVE';
}

function formatSwing(val) {
	return val ? 'MOVING' : 'STATIC';
}

function formatFan(val) {
	var text;
	
	switch (val) {
		case 0:
			text = 'AUTO';
		break;
		case 1:
			text = 'AUTO (SLEEP)';
		break;
		case 2:
			text = 'LOW';
		break;
		case 3:
			text = 'MEDIUM';
		break;
		case 5:
			text = 'HIGH';
		break;
	}
	
	return text;
}

function formatTimerValue(val) {
	return val / 6 + ' h';
}

function parseStatus(num) {
	var timerOn = num & 0x10;
	var timerOff = num & 0x08;
	var on = num & 0x04;
	
	return {
		timerOn: !!timerOn,
		timerOff: !!timerOff,
		on: !!on
	};
}

function parseMode(num) {
	var modeVal = num & 0x0F;

	switch (modeVal) {
		case 2:
			mode = 'DRY';
		break;
		case 3:
			mode = 'COOL';
		break;
		case 7:
			mode = 'FAN';
		break;
		case 8:
			mode = 'FEEL';
		break;
		default:
			mode = '? (' + modeVal + ')';
		break;
	}

	return {
		mode: mode,
	};
}

function parseTemperature(num) {
	return 31 - num;
}

function parseSettings(num) {
	var swing = num & 0x38;
	var fan = num & 0x07;
	
	var sleep = fan === 1;
	var timerOn = num & 0x20;
	var timerOff = num & 0x10;
	
	return {
		sleep: sleep,
		timerOn: !!timerOn,
		timerOff: !!timerOff,
		swing: !!swing,
		fan: fan
	}
}

function decode()
{
	// Update the content of user interface variables
    get_ui_vals();
	
	// Clears all the the decoder items and its content
    clear_dec_items();

	var colors = dark_colors;
	
	var bytes = pre_decode('ac_remote.js', [
		'ch = ' + ch,
		'msb = 0',
		'tolerance = 50',
		'init_low = 31',
		'init_high = 15',
		'data_t1 = 56',
		'data_t2_zero = 31',
		'data_t2_one = 110',
		'pause = 0'
	].join(';')).filter(function(value) {
		return value.data.length > 0;
	})

	if (bytes.length !== 14) {
		debug('Invalid length!');
		return;
	}
	
	var bytesData = bytes.map(function(b) {
		return parseInt(b.data);
	});
	
	var checksum = bytesData.slice(0, 13).reduce(function(prev, curr) {
		return prev + curr;
	}, 0) & 0xFF;
	
	if (checksum !== bytesData[13]) {
		debug('Invalid checksum (' + checksum + '), expected ' + bytesData[13]);
		return;
	}
	
	// BYTE 6
	var status = parseStatus(bytesData[5]);
	var color = status.on ? colors.green : colors.red;
	
	var timerOff = formatTimerValue(bytesData[9]);
	var timerOffColor = status.timerOff ? colors.green : colors.red;

	var timerOn = formatTimerValue(bytesData[10]);
	var timerOnColor = status.timerOn ? colors.green : colors.red;
	
	pkt_start();
	pkt_add_item(bytes[5].start_s, bytes[5].end_s, 'TURN', status.on ? 'ON' : 'OFF', color, color, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[5].start_s, bytes[5].end_s, 'TIMER OFF', timerOff, timerOffColor, timerOffColor, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'TIMER ON', timerOn, timerOnColor, timerOnColor, 0);
	pkt_end();
	
	// BYTE 7
	var mode = parseMode(bytesData[6]);
	
	pkt_start();
	pkt_add_item(bytes[6].start_s, bytes[6].end_s, 'MODE', mode.mode, colors.green, colors.green, 0);
	pkt_end();
	
	// BYTE 8
	var temperature = parseTemperature(bytesData[7]);

	pkt_start();
	pkt_add_item(bytes[7].start_s, bytes[7].end_s, 'TEMP', temperature, colors.green, colors.green, 0);
	pkt_end();
	
	// BYTE 9
	var settings = parseSettings(bytesData[8]);
	var fan = formatFan(settings.fan);
	var swing = formatSwing(settings.swing);
	var sleep = formatTimer(settings.sleep);

	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'SLEEP', sleep, colors.green, colors.green, 0);
	pkt_end();

	pkt_start();
	pkt_add_item(bytes[8].start_s, bytes[8].end_s, 'FAN', fan, colors.green, colors.green, 0);
	pkt_end();

	pkt_start();
	pkt_add_item(bytes[8].start_s, bytes[8].end_s, 'SWING', swing, colors.green, colors.green, 0);
	pkt_end();
}
