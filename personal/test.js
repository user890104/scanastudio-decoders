/* The decoder name
*/
function get_dec_name()
{
    return "Minimal decoder (example)"; 
}

/* The decoder version 
*/
function get_dec_ver()
{
    return "1.0";
}

/* Author 
*/
function get_dec_auth()
{
    return "IKALOGIC";
}

/* Graphical user interface for this decoder
*/
function gui()
{
    ui_clear();        // clean up the User interface before drawing a new one.
    ui_add_ch_selector("ch", "Channel", "Channel legend");
}

function decode()
{
    get_ui_vals();                // Update the content of user interface variables
    clear_dec_items();            // Clears all the the decoder items and its content

	var t = trs_get_first(ch);
	var old = t.sample;
	
	while (trs_is_not_last(ch)) {
		t = trs_get_next(ch);
		debug(t.sample - old);
		old = t.sample;
	}
}




