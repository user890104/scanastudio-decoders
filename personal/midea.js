/*
*************************************************************************************

							SCANASTUDIO 2 DECODER

The following commented block allows some related informations to be displayed online

<DESCRIPTION>

	Midea airconditioners' IR remote protocol

</DESCRIPTION>

<RELEASE_NOTES>

	V1.0:  Initial release

</RELEASE_NOTES>

<AUTHOR_URL>

	mailto:petar@isld.nl

</AUTHOR_URL>

<HELP_URL>



</HELP_URL>

*************************************************************************************
*/

/* The decoder name as it will apear to the users of this script 
*/
function get_dec_name()
{
	return "Midea AC IR remote";
}

/* The decoder version 
*/
function get_dec_ver()
{
	return "1.0";
}

/* Author 
*/
function get_dec_auth()
{
	return "Petar Velkov (init Lab)";
}

// Convert milliseconds to samples
function uSecToSamples(us) {
	var samples = us*sample_rate/1000000;
	
	return samples;
}

// Find initial sequence - 4ms down followed by 4ms up, returns the last transition before the first bit
function findInit() {
	get_ui_vals();
	var t = trs_get_first(ch), tNext;
	var len = 0;
	
	while(trs_is_not_last(ch)) {
		var tNext = trs_get_next(ch);
		
		if (t.val == 0 && tNext.sample - t.sample > uSecToSamples(4000)) {
			var tNextNext = trs_get_next(ch);
			
			if (tNextNext.sample - tNext.sample > uSecToSamples(4000)) {
				dec_item_new(ch, t.sample, tNextNext.sample);
				dec_item_add_pre_text("Init Sequence");
				dec_item_add_pre_text("INIT SEQ");
				dec_item_add_pre_text("init");
			
				return tNextNext;
			}
		}
		
		t = tNext;
	}
	
	return null;
}

/* Graphical user interface for this decoder 
*/
function gui()  //graphical user interface
{
	ui_clear();  // clean up the User interface before drawing a new one.
	ui_add_info_label( "Select the channel to which the IR receiver is connected." );
	ui_add_ch_selector( "ch", "Channel to decode:", "" );
}

function decode()
{
	get_ui_vals();                // Update the content of user interface variables
	clear_dec_items();            // Clears all the the decoder items and its content
	
	tInit = findInit();
	
	var t = trs_get_first(ch);
	while (t.sample != tInit.sample) {
		t = trs_get_next(ch);
	}
	
	if (tInit) {
		var output = "";
		var packet_started = true;
	
		var t = tInit;

//		debug("End of init sequence found, decoding packet...", tInit.sample);
	} else {
		debug("Cannot find Midea packets, aborting.")
		return;
	}
	
	while(packet_started) {
		if (t.val == 0) {
			var tBitStart = t;
			
//			debug("Starting to decode bit...", tBitStart.sample);

			var tNext = trs_get_next(ch);
			var T1 = tNext.sample - tBitStart.sample;
			debug("T1 = " + T1);
			if (T1 > uSecToSamples(400) && T1 < uSecToSamples(700)) {
				var tBitEnd = trs_get_next(ch);
				var T2 = tBitEnd.sample - tNext.sample;
				debug("T2 = " + T2);
				if (T2 > uSecToSamples(400) && T2 < uSecToSamples(600)) {
//					debug("Found a 0", tBitStart.sample);
					output += "0";
				} else if (T2 > uSecToSamples(1400) && T2 < uSecToSamples(1700)) {
//					debug("Found a 1", tBitStart.sample);
					output += "1";
				} else {
					packet_started = false;
//					debug("end bit, maybe?");
					dec_item_new(ch, tInit.sample, tBitStart.sample);
					dec_item_add_pre_text("Midea Config Sequence");
					dec_item_add_pre_text("Config");
					dec_item_add_data(parseInt(output));
	
					//output = output.slice(0, -1);
				}
				
				t = tBitEnd;
			} else {
				debug ("what the??????")
				packet_started = false;
			}
		}
	}
	
	debug(output);
//	debug(output.length);
	
	var hex = parseInt(output, 2).toString(16);
	
	debug(hex);
	
	
}









