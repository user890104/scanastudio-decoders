function get_dec_name()
{
	return "Hisense AC Remote";
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()  //graphical user interface
{
	ui_clear();  // clean up the User interface before drawing a new one.
	ui_add_ch_selector( "ch", "Channel", "IR" );
}

function sum(arr) {
	return arr.reduce(function(prev, curr) { return prev ^ curr; }, 0);
}

function formatNumber(num) {
	if (num < 10) {
		return '0' + num;
	}
	
	return num.toString();
}

function decode()
{
	// Update the content of user interface variables
    get_ui_vals();
	
	// Clears all the the decoder items and its content
    clear_dec_items();

	set_progress(0);

	var colors = dark_colors;
	
	var bytes = pre_decode('ac_remote.js', [
		'ch = ' + ch,
		'msb = 0',
		'tolerance = 30',
		'init_low = 90',
		'init_high = 45',
		'data_t1 = 55',
		'data_t2_zero = 55',
		'data_t2_one = 170',
		'pause = 80'
	].join(';')).filter(function(value) {
		return value.data.length > 0;
	});
	
	var messageLength = 21;

	if (bytes.length % messageLength !== 0) {
		debug('Invalid length!');
		return;
	}
	
	var messages = bytes.length / messageLength;
	
	debug('messages: ' + messages);

	for (i = 0; i < messages; i++) {
		var currBytes = bytes.slice(i * messageLength, (i + 1) * messageLength);
		var currBytesVal = currBytes.map(function(byte) { return byte.data[0]; });

		if (
			currBytesVal[0] !== 0x83 ||
			currBytesVal[1] !== 0x06 ||
			
			// 2: fan main
			// 3: high nibble = temperature
			currBytesVal[4] !== 0x00 ||
			currBytesVal[5] !== 0x00 ||
			
			// pause 8 ms
			
			// 6: low nibble = hour
			// 7: minutes
			currBytesVal[8] !== 0x00 ||
			currBytesVal[9] !== 0x00 ||
			currBytesVal[10] !== 0x00 ||
			currBytesVal[11] !== 0x00 ||
			currBytesVal[12] !== 0x00 ||
			// 13: xor of 2-12
			
			// pause 8 ms
			
			currBytesVal[14] !== 0x00 ||
			//currBytesVal[15] !== 0x02 ||
			currBytesVal[16] !== 0x00 ||
			// 17: fan sub step
			// 18: on/off
			currBytesVal[19] !== 0x00
			// 20: xor of 14-19
		) {
			debug('Invalid message');
			debug(currBytesVal.map(function(num) { return (num < 0x10 ? '0' : '') + num.toString(16).toUpperCase();}).join(' '));
			continue;
		}
		
		// BYTE 2, 17
		var fanMain = currBytesVal[2] & 0x03;
		var fanSub = (currBytesVal[17] & 0x40) > 0;
		
		switch (fanMain) {
			case 0:
				fan = 0;
				break;
			case 3:
				fan = fanSub ? 2 : 1;
				break;
			case 2:
				fan = 3;
				break;
			case 1:
				fan = fanSub ? 4 : 5;
				break;
			default:
				fan = 0;
				debug('fan unknown');
		}

		pkt_start();
		pkt_add_item(currBytes[2].start_s, currBytes[2].end_s, 'FAN', fan || 'AUTO', colors.green, colors.green, 0);
		pkt_end();

		// BYTE 3
		var temperature = (currBytesVal[3] >> 4) + 16;
	
		pkt_start();
		pkt_add_item(currBytes[3].start_s, currBytes[3].end_s, 'TEMP', temperature, colors.green, colors.green, 0);
		pkt_end();
		
		var modeNum = currBytesVal[3] & 0x07;
		
		switch (modeNum) {
			case 0:
				mode = 'HEATING';
				break;
			case 2:
				mode = 'COOLING';
				break;
			case 3:
				mode = 'DRY';
				break;
			case 4:
				mode = 'FAN ONLY';
				break;
			default:
				mode = 'UNKNOWN';
				debug('mode unknown');
		}
		
		pkt_start();
		pkt_add_item(currBytes[3].start_s, currBytes[3].end_s, 'MODE', mode, colors.green, colors.green, 0);
		pkt_end();

		// BYTE 6, 7
		var time = formatNumber(currBytesVal[6] & 0x1F) + ':' + formatNumber(currBytesVal[7]);
		
		pkt_start();
		pkt_add_item(currBytes[3].start_s, currBytes[5].end_s, 'TIME', time, colors.green, colors.green, 0);
		pkt_end();
		
		// BYTE 6, 16
		var dimmer = (currBytesVal[6] & 0x20) > 0 && (currBytesVal[16] & 0x02) === 0;		
		var color = dimmer ? colors.green : colors.red;
		
		pkt_start();
		pkt_add_item(currBytes[6].start_s, currBytes[6].end_s, 'DIMMER', on ? 'ON' : 'OFF', color, color, 0);
		pkt_end();

		// BYTE 13
		var cs1 = currBytesVal[13] === sum(currBytesVal.slice(2, 12));
		
		// BYTE 18
		var on = (currBytesVal[18] & 0x10) > 0;
		var color = on ? colors.green : colors.red;
		
		pkt_start();
		pkt_add_item(currBytes[18].start_s, currBytes[18].end_s, 'POWER', on ? 'ON' : 'OFF', color, color, 0);
		pkt_end();
		
		// BYTE 20
		var cs2 = currBytesVal[20] === sum(currBytesVal.slice(14, 19));
		
		debug('POWER ' + (on ? 'ON' : 'OFF') + ', MODE = ' + mode + ', TEMP = ' + temperature + ', FAN = ' + (fan || 'A') + ', TIME = ' + time + ', DIMMER = ' + (dimmer ? 'ON' : 'OFF') + ', CS1 ' + (cs1 ? 'OK' : 'MISMATCH') + ', CS2 ' + (cs2 ? 'OK' : 'MISMATCH'));
		set_progress((i + 1) / messages * 100);
	}
}


