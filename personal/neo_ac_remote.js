function get_dec_name()
{
	return 'NEO AC Remote';
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()
{
    ui_clear();
    ui_add_ch_selector('ch', 'Channel', 'IR');
}

function formatNumber(num) {
	if (num < 10) {
		return '0' + num;
	}
	
	return num.toString();
}

function formatTimer(val) {
	return val ? 'ACTIVE' : 'INACTIVE';
}

function formatVane(val) {
	return val ? 'MOVING' : 'STATIC';
}

function formatFan(val) {
	var text;
	
	switch (val) {
		case 0:
			text = 'AUTO';
		break;
		case 1:
			text = 'HIGH';
		break;
		case 2:
			text = 'MEDIUM';
		break;
		case 3:
			text = 'LOW';
		break;
	}
	
	return (4 - val) + ' (' + text + ')';
}

function parseMode(num) {
	var modes = [
		'COOL',
		'DRY',
		'VENT',
		'HEAT',
	];

	var setVane = num & 0x20;
	var on = num & 0x08;
	var mode = num & 0x07;
	
	return {
		on: !!on,
		mode: mode in modes ? modes[mode] : '?',
		setVane: !!setVane
	};
}

function parseTemperature(num) {
	return num - 9;
}

function parseSettings(num) {
	var timerSleep = num & 0x80;
	var timerOn = num & 0x20;
	var timerOff = num & 0x10;
	
	var vane = num & 0x08;
	var fan = num & 0x03;
	
	return {
		timerSleep: !!timerSleep,
		timerOn: !!timerOn,
		timerOff: !!timerOff,
		vane: !!vane,
		fan: fan
	}
}

function decode()
{
	// Update the content of user interface variables
    get_ui_vals();
	
	// Clears all the the decoder items and its content
    clear_dec_items();

	var colors = dark_colors;
	
	var bytes = pre_decode('ac_remote.js', [
		'ch = ' + ch,
		'msb = 0',
		'tolerance = 30',
		'init_low = 90',
		'init_high = 45',
		'data_t1 = 56',
		'data_t2_zero = 64',
		'data_t2_one = 172',
		'pause = 0'
	].join(';')).filter(function(value) {
		return value.data.length > 0;
	})

	if (bytes.length !== 9) {
		debug('Invalid length!');
		return;
	}
	
	var bytesData = bytes.map(function(b) {
		return parseInt(b.data);
	});
	
	if (bytesData[8] !== 0x0C) {
		debug('Invalid message');
		return;
	}
	
	// BYTE 0
	var mode = parseMode(bytesData[0]);
	var color = mode.on ? colors.green : colors.red;
	
	pkt_start();
	pkt_add_item(bytes[0].start_s, bytes[0].end_s, 'TURN', mode.on ? 'ON' : 'OFF', color, color, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[0].start_s, bytes[0].end_s, 'MODE', mode.mode, colors.green, colors.green, 0);
	pkt_end();
	
	if (mode.setVane) {
		pkt_start();
		pkt_add_item(bytes[0].start_s, bytes[0].end_s, 'SET VANE', '', colors.green, colors.green, 0);
		pkt_end();
	}
	
	// BYTE 1
	var temperature = parseTemperature(bytesData[1]);

	pkt_start();
	pkt_add_item(bytes[1].start_s, bytes[1].end_s, 'TEMP', temperature, colors.green, colors.green, 0);
	pkt_end();
	
	// BYTE 2
	var settings = parseSettings(bytesData[2]);
	var fan = formatFan(settings.fan);
	var vane = formatVane(settings.vane);
	var timerSleep = formatTimer(settings.timerSleep);
	var timerOn = formatTimer(settings.timerOn);
	var timerOff = formatTimer(settings.timerOff);

	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'TIMER SLEEP', timerSleep, colors.green, colors.green, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'TIMER ON', timerOn, colors.green, colors.green, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'TIMER OFF', timerOff, colors.green, colors.green, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'FAN', fan, colors.green, colors.green, 0);
	pkt_end();
	
	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'VANE', vane, colors.green, colors.green, 0);
	pkt_end();
	
	// BYTE 3, 4, 5
	var time = formatNumber(bytesData[5]) + ':' +
		formatNumber(bytesData[4]) + ':' +
		formatNumber(bytesData[3]);
	
	pkt_start();
	pkt_add_item(bytes[3].start_s, bytes[5].end_s, 'TIME', time, colors.green, colors.green, 0);
	pkt_end();
	
	// BYTE 6, 7
	var timer = formatNumber(bytesData[7]) + ':' +
		formatNumber(bytesData[6]);
	
	pkt_start();
	pkt_add_item(bytes[6].start_s, bytes[7].end_s, 'TIMER', timer, colors.green, colors.green, 0);
	pkt_end();
}
