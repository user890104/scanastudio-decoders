function get_dec_name()
{
	return 'Samsung AC Remote';
}

function get_dec_ver()
{
	return '1.0';
}

function get_dec_auth()
{
	return 'Vencislav Atanasov [user890104]';
}

function gui()
{
    ui_clear();
    ui_add_ch_selector('ch', 'Channel', 'IR');
}

function formatNumber(num) {
	if (num < 10) {
		return '0' + num;
	}
	
	return num.toString();
}

function fromGrayCode(gn) {
	if (gn < 0) {
		throw new RangeError('gray code numbers cannot be negative');
	}
	
	var g = gn.toString(2).split('');
	var b = [];
	b[0] = g[0];
	
	for (var i = 1; i < g.length; i++) {
		b[i] = g[i] ^ b[i - 1];
	}

	return parseInt(b.join(''), 2);
}

function formatFan(val) {
	var text;
	var fan = val >> 4;
	
	switch (fan) {
		case 0x1:
			text = 'AUTO/DRY';
		break;
		case 0x3:
			text = 'HIGH';
		break;
		case 0x9:
			text = 'LOW';
		break;
		case 0xB:
			text = 'AUTO';
		break;
		case 0xE:
			text = 'SLEEP';
		break;
		case 0xF:
			text = 'TURBO';
		break;
		default:
			text = 'UNKNOWN';
		break;
	}
	
	return fan + ' (' + text + ')';
}

function parseMode(num) {
	var mode = num & 0x0F;
	
	switch (mode) {
		case 0x0:
			return 'COOL';
		case 0x4:
			return 'DRY';
		case 0x8:
			return 'AUTO';
		case 0xC:
			return 'HEAT';
		default:
			return 'UNKNOWN';
	}
}

function parseTemperature(num) {
	return 17 + (num >> 4);
}

function decode()
{
	// Update the content of user interface variables
    get_ui_vals();
	
	// Clears all the the decoder items and its content
    clear_dec_items();

	var colors = dark_colors;
	
	var bytes = pre_decode('ac_remote.js', [
		'ch = ' + ch,
		'msb = 0',
		'tolerance = 30',
		'init_low = 30',
		'init_high = 90',
		'data_t1 = 54',
		'data_t2_zero = 50',
		'data_t2_one = 150',
		'pause = 0'
	].join(';')).filter(function(value) {
		return value.data.length > 0;
	})

	if (bytes.length !== 14) {
		debug('Invalid length (' + bytes.length + ')!');
		return;
	}
	
	var bytesData = bytes.map(function(b) {
		return parseInt(b.data);
	});
	/*
	var bytesCheck = bytesData.map(function(b) {
		return b > 127 ? b | 0xffffff00 : b;
	});
	
	var i;
	
	for (i = 0; i < bytesCheck.length; i += 2) {
		if (bytesCheck[i] !== (~bytesCheck[i + 1])) {
			debug('Error: byte ' + i + ' and byte ' + (i + 1) + ' do not match when inverted!');
			debug('byte[' + i + '] = ' + bytesCheck[i]);
			debug('byte[' + (i + 1) + '] = ' + bytesCheck[i + 1]);
			return;
		}
	}
	*/
	if (
		bytesData[0] !== 0x02 ||
		bytesData[1] !== 0x92 ||
		bytesData[2] !== 0x0F ||
		bytesData[3] !== 0x00 ||
		bytesData[4] !== 0x00 ||
		bytesData[5] !== 0x00 ||
		bytesData[6] !== 0xF0
	) {
		debug('Error: invalid start bytes');
		return;
	}
	
	var fan = bytesData[2] >> 4;
	var temp = bytesData[11] >> 4;
	var mode = bytesData[4] & 0x0F;
	/*
	// BYTE 2
	var fan = formatFan(bytesData[2]);

	pkt_start();
	pkt_add_item(bytes[2].start_s, bytes[2].end_s, 'FAN', fan, colors.green, colors.green, 0);
	pkt_end();
	*/
	// BYTE 4
	var mode = parseMode(bytesData[4]);
	var temperature = parseTemperature(bytesData[11]);
	//var color = true ? colors.green : colors.red;
	/*
	pkt_start();
	pkt_add_item(bytes[0].start_s, bytes[0].end_s, 'TURN', mode.on ? 'ON' : 'OFF', color, color, 0);
	pkt_end();
	*/
	/*
	pkt_start();
	pkt_add_item(bytes[0].start_s, bytes[0].end_s, 'MODE', mode, colors.green, colors.green, 0);
	pkt_end();
	*/
	pkt_start();
	pkt_add_item(bytes[11].start_s, bytes[11].end_s, 'TEMP', temperature, colors.green, colors.green, 0);
	pkt_end();
}
